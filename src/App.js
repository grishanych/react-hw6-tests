// import axios from "axios";
import FullScreenWrapper from "./components/FullScreenWrapper";
import "./main.scss";
import Product from "./components/Product";
import { useEffect, useState } from "react";
import Header from "./components/Header";
import Favorites from "./components/Favorites";
import { Route, Routes } from "react-router-dom";
import ProductsInCart from "./components/ProductsInCart";
import { useSelector, useDispatch } from 'react-redux';
// import { showProducts } from "./state/actions";
import { fetchProducts } from "./state/getProducts";

const style = {
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
};

const handleClick = (event) => {
  console.log(event.target);
};

function App() {
  const [favoriteCount, setFavoriteCount] = useState(0);
  const [cartCount, setCartCount] = useState(0);
  // const [products, setProducts] = useState([]);

  const [selectedCardId, setSelectedCardId] = useState(null);

  const [selectedProducts, setSelectedProducts] = useState([]);

  const [selectedProductsInCart, setSelectedProductsInCart] = useState([]);

  const state = useSelector((state) => state)
  const dispatch = useDispatch()


  const addToCartProducts = (product) => {
    setSelectedProductsInCart([...selectedProductsInCart, product]);
  };

  const addToSelectedProducts = (product) => {
    setSelectedProducts([...selectedProducts, product]);
  };

  useEffect(() => {
    const storedFavoriteCount = localStorage.getItem('favoriteCount');
    const storedInCartCount = localStorage.getItem('cartCount');



    if (storedFavoriteCount !== null) {
      setFavoriteCount(parseInt(storedFavoriteCount));
    }

    if (storedInCartCount !== null) {
      setCartCount(parseInt(storedInCartCount));
    }

    const storedSelectedProducts = localStorage.getItem("selectedProducts");
    if (storedSelectedProducts) {
      setSelectedProducts(JSON.parse(storedSelectedProducts));
    }
    const storedSelectedProductsInCart = localStorage.getItem("selectedProductsInCart");
    if (storedSelectedProductsInCart) {
      setSelectedProductsInCart(JSON.parse(storedSelectedProductsInCart));
    }

  }, []);


  // useEffect(() => {
  //   axios
  //     .get("/Products.json")
  //     .then((res) => {
  //       setProducts(res.data);
  //       localStorage.setItem("products", JSON.stringify(res.data));
  //     })
  //     .catch((err) => console.log(err));
  // }, []);

  // useEffect(() => {
  //   fetch('/Products.json')
  //     .then((response) => response.json())
  //     .then((data) => {
  //       dispatch(showProducts(data));
  //       // localStorage.setItem("products", JSON.stringify(data));
  //     })
  //     .catch((error) => {
  //       console.error('Помилка завантаження даних з JSON:', error);
  //     });
  // }, [dispatch]);

  useEffect(() => {
    // Замість використання fetch безпосередньо у компоненті, викликайте дію fetchProducts
    dispatch(fetchProducts());
  }, [dispatch]);

  useEffect(() => {
    const storedSelectedProductsInCart = localStorage.getItem('selectedProductsInCart');
    if (storedSelectedProductsInCart) {
      setSelectedProductsInCart(JSON.parse(storedSelectedProductsInCart));
    }

    const storedSelectedFavorites = localStorage.getItem('selectedProductsInCart');
    if (storedSelectedFavorites) {
      setSelectedProductsInCart(JSON.parse(storedSelectedFavorites));
    }

  }, []);

  useEffect(() => {
    localStorage.setItem('selectedProductsInCart', JSON.stringify(selectedProductsInCart));
  }, [selectedProductsInCart]);

  useEffect(() => {
    localStorage.setItem("selectedProducts", JSON.stringify(selectedProducts));
  }, [selectedProducts]);

  const updateFavoriteCount = (countChange) => {
    const newFavoriteCount = favoriteCount + countChange;

    if (newFavoriteCount >= 0) {
      setFavoriteCount(newFavoriteCount);
      localStorage.setItem("favoriteCount", newFavoriteCount.toString());
    } else if (newFavoriteCount <= 0) {
      setFavoriteCount(0);
    }
  };

  const updateCartCount = () => {
    const newCartCount = cartCount + 1;

    if (newCartCount >= 0) {
      setCartCount(newCartCount);
      localStorage.setItem("cartCount", newCartCount.toString());
    }
  };

  const removeProduct = (id) => {
    const deleteCard = window.confirm("Delete this card?");
    if (deleteCard === true) {
      state(state.filter((p) => p.id !== id));
    } else {
      state(state);
    }
  };

  const checkProductId = (id) => {
    setSelectedCardId(id);
    console.log(id);
  };

  const removeFromCart = (productId) => {
    const updatedSelectedProductsInCart = selectedProductsInCart.filter(
      (product) => product.id !== productId
    );
    setSelectedProductsInCart(updatedSelectedProductsInCart);
    setCartCount(cartCount - 1)
  };

  const productsElements = state ? (
    state.productList.map((p) => (
      <Product
        key={p.id}
        info={p}
        updateFavoriteCount={updateFavoriteCount}
        updateCartCount={updateCartCount}
        handleClick={handleClick}
        deleteHandler={removeProduct}
        checkProductId={checkProductId}
        addToSelectedProducts={addToSelectedProducts}
        addToCartProducts={addToCartProducts}
      />
    ))
  ) : (
    <p>Loading...</p>
  );



  // const productsElements = state.map((p) => (
  //   <Product
  //     key={p.id}
  //     info={p}
  //     updateFavoriteCount={updateFavoriteCount}
  //     updateCartCount={updateCartCount}
  //     handleClick={handleClick}
  //     deleteHandler={removeProduct}
  //     checkProductId={checkProductId}
  //     addToSelectedProducts={addToSelectedProducts}
  //     addToCartProducts={addToCartProducts}
  //   />
  // ));

  return (
    <>
      <Header favoriteCount={favoriteCount} cartCount={cartCount} />
      <Routes>
        <Route
          path="/"
          element={
            <FullScreenWrapper>
              <div style={style}>{productsElements}</div>
            </FullScreenWrapper>
          }
        />
        <Route
          path="/cart"
          element={
            <ProductsInCart
              selectedProducts={selectedProductsInCart}
              deleteHandler={removeFromCart}
            />
          }
        />
        <Route
          path="/favorites"
          element={
            <Favorites
              selectedProducts={selectedProducts}
              deleteHandler={removeProduct}
            />
          }
        />
        <Route path="*" element={<div>Not found Route</div>} />
      </Routes>
    </>
  );
}

export default App;
