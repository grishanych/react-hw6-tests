import defaultReducer from "../state/reducer";
import { SHOW_PRODUCTS, SET_MODAL_STATUS } from "../state/actions";

describe('defaultReducer', () => {
    it('should handle SHOW_PRODUCTS action', () => {
      const initialState = { productList: [] };
      const products = [{ id: 1, name: 'Product 1' }];
  
      const action = {
        type: SHOW_PRODUCTS,
        payload: products,
      };
  
      const newState = defaultReducer(initialState, action);
  
      expect(newState).toEqual({ productList: products });
    });
  
    it('should handle SET_MODAL_STATUS action', () => {
      const initialState = { modalStatus: true };
      const newStatus = true;
  
      const action = {
        type: SET_MODAL_STATUS,
        payload: newStatus,
      };
  
      const newState = defaultReducer(initialState, action);
  
      expect(newState).toEqual({ modalStatus: newStatus });
    });
  
    it('should return the same state for an unknown action', () => {
      const initialState = { productList: [], modalStatus: false };
  
      const action = {
        type: 'UNKNOWN_ACTION',
        payload: 'some_data',
      };
  
      const newState = defaultReducer(initialState, action);
  
      expect(newState).toEqual(initialState);
    });
  });