import {render, screen} from '@testing-library/react';
import Button from '../components/Button';
import  Form from '../components/Form';
import Product from '../components/Product';
import Modal from '../components/Modal';




test('render button', () =>{
    render(<Button />);
    const component = screen.getByRole('button');
    expect(component).toBeInTheDocument()
})

test('check button name', () =>{
    render(<Form />);
    const comp = screen.getByText('Checkout')
    expect(comp).toBeInTheDocument()
})


describe('Modal Component', () => {
    it('renders modal with header', () => {
      const mockHeader = 'Sample Header';
  
      render(<Modal header={mockHeader} showModal={true} setShowModal={() => {}} />);
  
      const modalHeader = screen.getByText(mockHeader);
      expect(modalHeader).toBeInTheDocument();
    });
  });