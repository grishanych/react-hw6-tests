import {SHOW_PRODUCTS} from './actions';
// import { combineReducers } from 'redux';
// import modalReducer from './modalReducer';

function defaultReducer(state,  action) {
    switch (action.type) {
        case SHOW_PRODUCTS:
            return {...state, productList: [...action.payload]}
        default:
            return state

    }
}


// const rootReducer = combineReducers({
//     products: defaultReducer,
//     modal: modalReducer
// })

export default defaultReducer