
import { SET_MODAL_STATUS } from "./actions";


const initialState = {
    isModalOpen: false, 
  };
  
  function modalReducer(state = initialState, action) {
    switch (action.type) {
      case SET_MODAL_STATUS:
        return {
          ...state,
          isModalOpen: action.payload,
        };
      default:
        return state;
    }
  }
  
  export default modalReducer;