export const SHOW_PRODUCTS = 'SHOW_PRODUCTS';
export const SET_MODAL_STATUS = 'SET_MODAL_STATUS';

export function showProducts(products) {
  return {
    type: SHOW_PRODUCTS,
    payload: products,
  };
}


export function setModalStatus(status) {
  return {
    type: SET_MODAL_STATUS,
    payload: status
  };
}