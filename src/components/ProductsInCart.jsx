// Компонент для відображення вибраних товарів
import React from 'react';
import styles from './styles/ProductsInCart.module.scss'
import Form from './Form';

function ProductsInCart(props) {
  const handleCardDelete = (productId) => {
  props.deleteHandler(productId)
}

  return (
    <div >
      <Form />
      <h2>Products in Cart:</h2>
      <ul className={styles.card}>
        {props.selectedProducts.map((product) => (
          <li className={styles.productCard} key={product.id}>
            <h3> {product.brandName}</h3>
            <p>{product.price} $</p>
          <img className={styles.selectedImg} src={product.image} alt="" />
          <button onClick={() => handleCardDelete(product.id)}>Delete Card</button> 
          </li>
        ))}
      </ul>
     
    </div>
  );
}



export default ProductsInCart;
