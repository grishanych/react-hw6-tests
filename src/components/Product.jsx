import { useEffect, useState } from "react";
import Button from './Button';
import styles from './styles/Products.module.scss'
import style from './styles/Modal.module.scss'
import Modal from "./Modal";
import PropTypes from 'prop-types';
import { useViewMode } from "./ViewModeContext";



function Product(props) {

  const [showModal, setShowModal] = useState(false);
  const [isFavorite, setIsFavorite] = useState(false);
  const [isInCart, setisInCart] = useState(false);

  const {viewMode, toggleViewMode} = useViewMode();

  useEffect(() => {
    const storedFavoriteStatus = localStorage.getItem(`favoriteStatus_${props.info.id}`);

    const storedInCartStatus = localStorage.getItem(`inCartStatus_${props.info.id}`);

    if (storedFavoriteStatus !== null) {
      setIsFavorite(JSON.parse(storedFavoriteStatus));
    }

    if (storedInCartStatus !== null) {
      setisInCart(JSON.parse(storedInCartStatus));
    }
  }, [props.info.id]);




  useEffect(() => {
    localStorage.setItem(`inCartStatus_${props.info.id}`, JSON.stringify(isInCart));
    localStorage.setItem(`favoriteStatus_${props.info.id}`, JSON.stringify(isFavorite));
  }, [props.info.id, isInCart, isFavorite]);

  const handleFavoriteClick = () => {
    setIsFavorite(prevIsFavorite => !prevIsFavorite);
    props.updateFavoriteCount(isFavorite ? -1 : 1);
  };

  const handleAddToCartClick = () => {
    const newIsInCart = !isInCart;
    setisInCart(newIsInCart);
    props.updateCartCount(newIsInCart ? 1 : -1);
    props.addToCartProducts(props.info);
  };

  const openModal = () => {
    setShowModal(true)
  }

  const cancelButton = (
    <button className={style.modalButton} onClick={() => {

      setShowModal(false)
    }}>Cancel</button>
  );

  const saveButton = (
    <button className={style.modalButton}
      onClick={() => { handleAddToCartClick(); setShowModal(false) }}
    >Ok</button>
  );

  const actionButtons = [saveButton, cancelButton];


  return (
    <>
      <div className={styles.card}>
        

        <button className={`${styles.favoriteButton} ${isFavorite ? styles.favorite : styles.star}`} onClick={() => { handleFavoriteClick(); props.checkProductId(props.info.id); props.addToSelectedProducts(props.info) }}>
          <span>&#9733;</span>
        </button>
        <h2>{props.info.brandName}</h2>
        <p>{props.info.price} $</p>
        <img className={styles.imgCar} src={props.info.image} alt="" />
        <Button backgroundColor='lightgreen' text='Add to cart' heandler={openModal} />
        {/* <button className={styles.deleteBtn} onClick={() => props.deleteHandler(props.info.id)}>Delete Card</button> */}

        <button onClick={toggleViewMode}>
        Toggle View Mode: {viewMode === 'cards' ? 'Table' : 'Cards'}
      </button>
      </div>

      <Modal
        header='Add to cart this product?'
        text='Are you sure you want to  cart this product?'
        actions={actionButtons}
        showModal={showModal}
        setShowModal={setShowModal}
        closeButton={() => setShowModal(false)}
      />
    </>

  )
}

Product.propTypes = {
  info: PropTypes.shape({
    id: PropTypes.number.isRequired,
    brandName: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired
  }).isRequired,
  updateFavoriteCount: PropTypes.func.isRequired,
  updateCartCount: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
  deleteHandler: PropTypes.func.isRequired,
  checkProductId: PropTypes.func.isRequired
}

Product.defaultProps = {
  updateFavoriteCount: () => { },
  updateCartCount: () => { },
  handleClick: () => { },
  deleteHandler: () => { },
  checkProductId: () => { },
};

export default Product