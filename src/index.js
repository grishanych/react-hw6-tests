import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
import {Provider} from 'react-redux'
import store from './state/store'
import { ViewModeProvider } from './components/ViewModeContext';



const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <ViewModeProvider>
    <Provider store={ store }>
    <BrowserRouter>
    <App />
    </BrowserRouter>
    </Provider>
    </ViewModeProvider>
);

